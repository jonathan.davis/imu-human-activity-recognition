import paho.mqtt.client as mqtt
import secrets as _s

file_handle = None

def on_connect(client, userdata, flags, rc):
    global file_handle
    
    print(f"Connected with result code {rc}")
    print(f'Opening data file')
    file_handle = open(f'{_s.ACTIVITY}.json', 'wb')
    client.subscribe(_s.ACTIVITY)


def on_message(client, userdata, msg):
    print(f"{msg.topic}: {msg.payload}")
    file_handle.write(msg.payload)
    file_handle.write(b"\n")


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.0.182", 1883, keepalive=60)

try:
    client.loop_forever()
except KeyboardInterrupt:
    print('Closing file and exiting')
    file_handle.close()