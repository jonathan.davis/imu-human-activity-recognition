import busio
import board
import wifi
import adafruit_requests as requests
import adafruit_minimqtt.adafruit_minimqtt as minimqtt
import socketpool
import ssl
import os
import time
import json
import secrets as _s
from digitalio import DigitalInOut, Direction, Pull
from adafruit_icm20x import ICM20948, AccelRange, GyroRange
from adafruit_hcsr04 import HCSR04
from adafruit_dps310.basic import DPS310

# Configuration constants
USE_MOSQUITTO: bool = True

# Connect to wifi
def connect_to_wifi():
    print("Connecting to network...")
    wifi.radio.connect(ssid=_s.WIFI_SSID,password=_s.WIFI_PASSWORD)
    print("Connected to: " + _s.WIFI_SSID + ", IP address: " + str(wifi.radio.ipv4_address))


# MQTT Functions
def connect_MQTT(client, userdata, flags, rc):
    print("Connected to {0}".format(client.broker))

    
def disconnect_MQTT(client, userdata, rc):
    print("Disconnected from {0}".format(client.broker))

    
def subscribe_MQTT(mqtt_client, userdata, topic, granted_qos):
    print("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))

    
def unsubscribe_MQTT(mqtt_client, userdata, topic, pid):
    print("Unsubscribed from {0} with PID {1}".format(topic, pid))


def publish_MQTT(client, userdata, topic, pid):
    print("Published to {0} with PID {1}".format(topic, pid))

    
def message_MQTT(client, topic, message):
    print("New message on topic {0}: {1}".format(topic, message))


# Init push button
btn = DigitalInOut(board.GP0)
btn.direction = Direction.INPUT
btn.pull = Pull.UP
prev_btn_state = btn.value

# Init LED
led = DigitalInOut(board.LED)
led.direction = Direction.OUTPUT

# Init board peripherals
i2c = busio.I2C(scl=board.GP27, sda=board.GP26)
sonar = HCSR04(trigger_pin=board.GP28, echo_pin=board.GP22)

imu_1 = ICM20948(i2c)
# Use smaller ranges for higher accuracy
imu_1.accelerometer_range = AccelRange.RANGE_4G
imu_1.gyro_range = GyroRange.RANGE_500_DPS
imu_1.accelerometer_data_rate = 125 # Hz; divisor = 8
imu_1.gyro_data_rate = 100 # Hz; divisor = 10

imu_2 = ICM20948(i2c, address=0x68)
# Use smaller ranges for higher accuracy
imu_2.accelerometer_range = AccelRange.RANGE_4G
imu_2.gyro_range = GyroRange.RANGE_500_DPS
imu_2.accelerometer_data_rate = 125 # Hz; divisor = 8
imu_2.gyro_data_rate = 100 # Hz; divisor = 10

baro = DPS310(i2c)

pool = socketpool.SocketPool(wifi.radio)
context = ssl.create_default_context()
    
# Configure MQTT client between mosquitto and adafruit
if USE_MOSQUITTO:
    mqtt_client = minimqtt.MQTT(
        broker = '192.168.0.182',
        port = 1883,
        socket_pool = pool,
        ssl_context = context
        )
else:
    mqtt_client = minimqtt.MQTT(
        broker = 'io.adafruit.com',
        port = 1883,
        username = _s.MQTT_USERNAME,
        password = _s.MQTT_PASSWORD,
        socket_pool = pool,
        ssl_context = context
        )

mqtt_client.on_connect = connect_MQTT
mqtt_client.on_disconnect = disconnect_MQTT
mqtt_client.on_subscribe = subscribe_MQTT
mqtt_client.on_unsubscribe = unsubscribe_MQTT
mqtt_client.on_publish = publish_MQTT
mqtt_client.on_message = message_MQTT

connect_to_wifi()
mqtt_client.connect()

readings = []
should_send_data = False
last_send_time = time.monotonic()

while True:
    cur_btn_state = btn.value
    if cur_btn_state != prev_btn_state:
        if not cur_btn_state:
            # toggle data flow
            should_send_data = not should_send_data
            led.value = should_send_data
            if should_send_data:
                last_send_time = time.monotonic()
            else:
                readings.clear()
    prev_btn_state = cur_btn_state

    # Read sensor data
    accel_1 = imu_1.acceleration
    gyro_1 = imu_1.gyro
    accel_2 = imu_2.acceleration
    gyro_2 = imu_2.gyro
    pressure = baro.pressure
    try:
        distance = sonar.distance
    except RuntimeError:
        pass

    # Send to logger
    if should_send_data:
        cur_time = time.monotonic()

        data = {
            "time": cur_time,
            "imu_1_x": accel_1[0],
            "imu_1_y": accel_1[1],
            "imu_1_z": accel_1[2],
            "imu_1_pitch": gyro_1[0],
            "imu_1_roll": gyro_1[1],
            "imu_1_yaw": gyro_1[2],
            "imu_2_x": accel_2[0],
            "imu_2_y": accel_2[1],
            "imu_2_z": accel_2[2],
            "imu_2_pitch": gyro_2[0],
            "imu_2_roll": gyro_2[1],
            "imu_2_yaw": gyro_2[2],
            "distance": distance,
            "pressure": pressure,
        }

        try:
            mqtt_payload = json.dumps(data)
            mqtt_client.publish(_s.ACTIVITY, mqtt_payload, retain=True)
        except BrokenPipeError:
            mqtt_client.reconnect()
